// Modules to control application life and create native browser window

const {app, BrowserWindow, ipcMain, dialog } = require('electron');
const excel_lib = require('xlsx');
const windowsshortcuts = require('windows-shortcuts');
const fs = require('fs');
const request = require('request');
const { maskBr } = require('js-brasil');
const Cloudant =  require ( '@cloudant/cloudant' ); 
var cloudant = null;
var db = null;

const urlApiHomologacao = "http://arko.devemandamento.com/arkowebyii";
const urlApiProducao = "http://arkoweb.com";


// const {download} = require('electron-dl');
// const { dialog } = require('electron').remote.dialog;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let telaAddArquivo;
let telaLogin;
let notificacao;

var usuario_logado = 'Default';
let loja;
let inventario;


// ------------------- CODIGO NECESSARIO PARA GERAR O INSTALADOR - INICIO ---------------
// handling squirrel events
if (require('electron-squirrel-startup')) return;
 
// this should be placed at top of main.js to handle setup events quickly
if (handleSquirrelEvent()) {
   // squirrel event handled and app will exit in 1000ms, so don't do anything else
   return;
}
function handleSquirrelEvent() {
   if (process.argv.length === 1) {
     return false;
   }
   const ChildProcess = require('child_process');
   const path = require('path');
   const appFolder = path.resolve(process.execPath, '..');
   const rootAtomFolder = path.resolve(appFolder, '..');
   const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
   const exeName = path.basename(process.execPath);
   const spawn = function(command, args) {
      let spawnedProcess, error;
      try {
        spawnedProcess = ChildProcess.spawn(command, args, {detached: true});
      } catch (error) {}
        return spawnedProcess;
      };
   const spawnUpdate = function(args) {
     return spawn(updateDotExe, args);
   };
   const squirrelEvent = process.argv[1];
   switch (squirrelEvent) {
     case '--squirrel-install':
     case '--squirrel-updated':
       // Optionally do things such as:
       // - Add your .exe to the PATH
       // - Write to the registry for things like file associations and
       //   explorer context menus
        // Install desktop and start menu shortcuts
       spawnUpdate(['--createShortcut', exeName]);
        setTimeout(app.quit, 1000);
       return true;
      case '--squirrel-uninstall':
       // Undo anything you did in the --squirrel-install and
       // --squirrel-updated handlers
        // Remove desktop and start menu shortcuts
       spawnUpdate(['--removeShortcut', exeName]);
        setTimeout(app.quit, 1000);
       return true;
      case '--squirrel-obsolete':
       // This is called on the outgoing version of your app before
       // we update to the new version - it's the opposite of
       // --squirrel-updated
        app.quit();
       return true;
   }
};

// ------------------- CODIGO NECESSARIO PARA GERAR O INSTALADOR - FIM ---------------

// tela ERRO
function createWindowTelaErro(){
  telaLogin = new BrowserWindow({
    width: 400, 
    height: 500,
    frame: false,
    show: false,
    icon: __dirname+'/images/icon.png',
    resizable: false,
    title: "Erro",
    webPreferences: {
      nodeIntegration: true,
    }
  })

  telaLogin.once("ready-to-show", () => {
    telaLogin.show();
  })

  telaLogin.loadFile('./views/erro.html');

  // Open the DevTools.
  // telaLogin.webContents.openDevTools()

  // Emitted when the window is closed.
  telaLogin.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    telaLogin = null
  })
}
// Tela LOGIN
function createWindowTelaLogin(){
  telaLogin = new BrowserWindow({
    width: 400, 
    height: 500,
    frame: false,
    show: false,
    icon: __dirname+'/images/icon.png',
    resizable: false,
    title: "Login",
    webPreferences: {
      nodeIntegration: true,
    }
  })

  telaLogin.once("ready-to-show", () => {
    telaLogin.show();
  })

  telaLogin.loadFile('./views/login.html');

  // Open the DevTools.
  // telaLogin.webContents.openDevTools()

  // Emitted when the window is closed.
  telaLogin.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    telaLogin = null
  })
}

// TELA PRINCIPAL
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1300, 
    height: 700, 
    frame: false, 
    show: false,
    icon: __dirname+'/images/icon.png',
    webPreferences: {
      nodeIntegration: true,
    }
  })  

  mainWindow.once("ready-to-show", () => {
    mainWindow.show();
  })

  // and load the index.html of the app.
  // mainWindow.loadFile('./views/index.html');

  // arquivo novo layout
  // mainWindow.loadFile('./views/newarquivo.html');
  // mainWindow.loadFile('./views/dashboard.html');
  mainWindow.loadFile('./views/dashboardComDataTable.html');

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// TELA ADD ARQUIVO
function createWindowTelaAddArquivo(){
  telaAddArquivo = new BrowserWindow({
    width: 400, 
    height: 418,
    frame: false,
    show: false,
    resizable: false,
    title: "tela add arquivo"
  })

  telaAddArquivo.once("ready-to-show", () => {
    telaAddArquivo.show();
  })

  telaAddArquivo.loadFile('./views/formAddArquivo.html');

  // Open the DevTools.
  // telaAddArquivo.webContents.openDevTools();

  // Emitted when the window is closed.
  telaAddArquivo.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    telaAddArquivo = null
  })
}



// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// DETERMINA QUAL TELA SERA INICIADA NA EXECUÇÃO DO SISTEMA
app.on('ready', () => {
  // abrir tela principal
  // createWindow();

  // pinga o servidor de base, se retornar conecta e abre o login, se não, entra na tela de erro
  const cloudant = Cloudant(
    {username:'11484c5d-c670-4537-a549-df992c9dac2c-bluemix',
     password:'3ed768cf71279dbf7392f874cb27989a55cfba8bcb492c029427f4e1aa967371',
     url:"https://11484c5d-c670-4537-a549-df992c9dac2c-bluemix:3ed768cf71279dbf7392f874cb27989a55cfba8bcb492c029427f4e1aa967371@11484c5d-c670-4537-a549-df992c9dac2c-bluemix.cloudantnosqldb.appdomain.cloud/arko_server_producao_3"},
     
    (err,cloudant, pong)=>{
      if (err) {
        // abrir tela erro
        createWindowTelaErro();
        return console.log('Failed to initialize Cloudant: ' + err.message);
      }
      if(cloudant){
        console.log(cloudant)
        console.log('cloudant')
      }
      db = cloudant.db.use("arko_server_producao_3");
      // abrir tela login
      createWindowTelaLogin();
      console.log(pong); // {"couchdb":"Welcome","version": ...
    });
  
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})



// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.


// funções

ipcMain.on('getUsuarioLogado', (event) =>{
  let usuario = { nome: usuario_logado };
  mainWindow.send('setUsuarioLogado', usuario);
  console.log(usuario);
});

function convertExcelToJson(path, callBack){
  var url = path;

  var workbook = excel_lib.readFile(url);
  let plan_pages = workbook.SheetNames;
  let plan_json = excel_lib.utils.sheet_to_json(workbook.Sheets[plan_pages[0]], {raw: true, defval:null});
  return plan_json;
}

ipcMain.on('getLojas', (event) => {
  let lojas = [];

  try {
    let requisicao = request(urlApiProducao+'/web/server/loja/index', function (error, response, body) {
    // let requisicao = request(urlApiHomologacao +'/web/server/loja/index', function (error, response, body) {
      if (!error && response.statusCode == 200) {
        // console.log(body) // Aqui podes ver o HTML da página pedida. 
        let json = JSON.parse(body);
        
        json.forEach(function(loja){
          if(loja.status == 'ATIVO'){
            lojas.push({id:loja.id, nome:loja.nome_fantasia});
          }
        });
        console.log(lojas);
        telaAddArquivo.send('setLojas',lojas);
      }
    }); 
    // console.log(lojas);

    requisicao.on('error', (e) => {
      console.log('Houve um erro: '+e.message);
      telaAddArquivo.send('erroRequisicao');
    });

    requisicao.end();  
  } catch (error) {
    console.log('caiu no catch - lojas');
    telaAddArquivo.send('erroRequisicao');
  }

  

});

ipcMain.on('getInventarios', (event, loja) => {
  let inventarios = [
    // {id:"22", nome:"Inventario 1"},
    // {id:"22", nome:"Inventario 2"}
  ];

  try {
    // http://localhost/arkowebyii/web/loja/getinventarios?
    let requisicao = request(urlApiProducao+'/web/loja/getinventarios?id='+loja, function (error, response, body) {
    // let requisicao = request(urlApiHomologacao+'/web/loja/getinventarios?id='+loja, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body) // Aqui podes ver o HTML da página pedida. 
        let json = JSON.parse(body);
        
        json.forEach(function(inventario){
          inventarios.push({id:inventario.id, nome:inventario.numero_inventario});
        });
        console.log(inventarios);
        telaAddArquivo.send('setInventarios',inventarios);
      }
    }); 
      // console.log(lojas);

      

    requisicao.on('error', (e) => {
      console.log('Houve um erro: '+e.message);
      console.log('caiu no catch - inventarios');
      telaAddArquivo.send('erroRequisicao');
    });

    requisicao.end();

  } catch (error) {
    console.log('caiu no catch - inventarios');
    telaAddArquivo.send('erroRequisicao');
  }

  
});


// salva os arquivos renderizados da tabela em um txt com json com todos os registros
ipcMain.on('exportarTabelaformatada', (event, json) => {
  let tipo = 'success';
  let titulo = 'Salvo';
  let texto = 'Arquivo salvo com sucesso';
  try {
    dialog.showSaveDialog(mainWindow, (filename)=>{
      try{
        let tabela;
        let wb;
        let fileName;
        let item;

        fs.writeFile(filename+".txt", JSON.stringify(json), function(err) {
            if (err){
                console.log(err);
            }
        });
    
        // tabela = excel_lib.utils.json_to_sheet(
        //   json,
        //   {
        //     header:
        //       [
        //         "linha",
        //         "cod_barras",
        //         "cod_interno",
        //         "departamento",
        //         "descricao_setor_secao",
        //         "familia",
        //         "grupo",
        //         "referencia",
        //         "saldo_qtd_estoque",
        //         "setor_secao",
        //         "subfamilia",
        //         "valor_custo",
        //         "valor_venda",
        //         "descricao_item"
        //       ], 
        //     skipHeader:false
        //   });
        
        // wb = excel_lib.utils.book_new();
        // excel_lib.utils.book_append_sheet(wb, tabela, 'erros');
    
        // excel_lib.writeFile(wb, filename);
        mainWindow.send('exportarTabelaformatada',tipo, titulo ,texto);
      }catch(err){

      }
    });    
  } catch (error) {
    tipo = 'danger';
    titulo = 'Erro';
    texto = 'Ocorreu algo de errado, tente novamente.'
    mainWindow.send('exportarTabelaformatada',tipo, titulo ,texto);
    
  }
  
});


ipcMain.on('exportarErros', (event, json_erros) => {
  let tipo = 'success';
  let titulo = 'Salvo';
  let texto = 'Arquivo salvo com sucesso';
  try {
    dialog.showSaveDialog(mainWindow, (filename)=>{
      try{
        let tabela;
        let wb;
        let fileName;
        let item;
    
        tabela = excel_lib.utils.json_to_sheet(
          json_erros,
          {
            header:
              [
                "linha",
                "cod_barras",
                "cod_interno",
                "departamento",
                "descricao_setor_secao",
                "familia",
                "grupo",
                "referencia",
                "saldo_qtd_estoque",
                "setor_secao",
                "subfamilia",
                "valor_custo",
                "valor_venda",
                "descricao_item"
              ], 
            skipHeader:false
          });
        
        wb = excel_lib.utils.book_new();
        excel_lib.utils.book_append_sheet(wb, tabela, 'erros');
    
        excel_lib.writeFile(wb, filename+'.xlsx');
        mainWindow.send('exportarErros',tipo, titulo ,texto);
      }catch(err){

      }
    });    
  } catch (error) {
    tipo = 'danger';
    titulo = 'Erro';
    texto = 'Ocorreu algo de errado, tente novamente.'
    mainWindow.send('exportarErros',tipo, titulo ,texto);
    
  }
  
});

ipcMain.on('arquivoImportado', (event, selecionados, arquivo) => {

  let convertido = false;
  let plan_json = convertExcelToJson(arquivo); 

  console.log('selecionados');
  console.log(selecionados);

do {
  if(plan_json){
    // console.log(plan_json);
    convertido = true;
    // extrai as keys do json para comparar com o layout padrão trabalhado no sistema
    if(validaColunasExcel(Object.keys(plan_json[0]))){
      let cont = 0
      console.log('iniciou processo, aguarde...')
      plan_json.forEach(function(item){
        Object.assign(item,{loja:selecionados[0].id,inventario:selecionados[1].id});
        cont++
        // console.log(item);
        });
      console.log('Finalizado, total de itens: '+cont);
      mainWindow.webContents.send('planilhaEmJson', plan_json);
    }else{
      mainWindow.webContents.send('erroPlanilha');
    }
    telaAddArquivo.webContents.send('carregouPlanilha', convertido);
  }else{
    console.log('ainda não converteu');
  }
} while(convertido != true);
  
});

ipcMain.on('abrirJanelaAddArquivo',() => {
  createWindowTelaAddArquivo();
})

// login estatico, fazer e conectar no banco
ipcMain.on('realizarLogin', (event, login) => {

  try {

    let cpf = maskBr.cpf(login[0].usuario); 
    
    let requisicao = request(urlApiProducao+'/web/site/logindesktop?cpf='+cpf+'&password='+login[0].senha, function (error, response, body) {
    // let requisicao = request(urlApiHomologacao+'/web/site/logindesktop?cpf='+cpf+'&password='+login[0].senha, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        // console.log(body) // Aqui podes ver o HTML da página pedida. 
        let json = JSON.parse(body);
        console.log(body);
        if(json.status == 'ok'){
          usuario_logado = json.username;
          console.log('usuario logado: '+usuario_logado);
          createWindow();
          telaLogin.send('loginExistente');
        }else{
          telaLogin.send('loginInexistente');
        }
      }
    }); 
      // console.log(lojas);

    requisicao.on('error', (e) => {
      console.log('Houve um erro: '+e.message);
      console.log('caiu no catch - inventarios');
      // telaAddArquivo.send('erroRequisicao');
    });

    requisicao.end();

  } catch (error) {
    console.log('caiu no catch - inventarios');
    // telaAddArquivo.send('erroRequisicao');
  }

  // users.forEach(user => {
  //   if(login[0].usuario == user.usuario){
  //       if(login[0].senha == user.senha){
  //           // usuario_logado = user.usuario;
  //           // console.log('usuario logado: '+usuario_logado);
  //           // createWindow();
  //           // telaLogin.send('loginExistente')
  //       }
  //   }
  // });
})

// LOG - falta determinar as informações necessarias no log e persistir em uma base
ipcMain.on('registrarLog',(event, doc)=>{
  let log = {usuario:usuario_logado, acao: doc.acao, erro:doc.erro, data: doc.data};
  console.log(log);
})

ipcMain.on('iniciarTelaPrincipal',()=>{
  createWindow();
});

// verifica se a disposição das colunas do excel estão de acordo com o padrão trabalhado no sistema
// return
// true => colunas na disposição correta
// false =>  colunas na disposição errada
function validaColunasExcel(chaves){
  let colunas = [
    'cod_barras',
    'cod_interno',
    'departamento',
    'setor_secao',
    'descricao_setor_secao',
    'grupo',
    'familia',
    'subfamilia',
    'referencia',
    'saldo_qtd_estoque',
    'valor_custo',
    'valor_venda',
    'descricao_item'
  ];

  let erro = false;

  try {
    for(let i = 0; i < chaves.length; i++){
      if( colunas[i] != undefined){
        if(chaves[i] != colunas[i]){
          console.log('colunas na disposição errada: ');
          console.log('chaves: '+chaves[i] +'| Coluna: '+colunas[i]);
          erro = true;
        }else{
          console.log('chaves: '+chaves[i] +'| Coluna: '+colunas[i]);
        }
      }
    }
  } catch (error) {
    console.log('inconformidade no layout da tabela submetida');
  }
  

  if(erro == true){
    console.log('colunas na disposição errada');
    return false;
  }else{
    console.log('tudo certo');
    return true;
  }

}

ipcMain.on('importarDados',(event,json_dados)=>{
  const documents = json_dados;
  if(db){
    db.bulk({docs:documents}, (err)=>{
      if(!err){
        console.log('printou o body');
        mainWindow.send('importarDadosEnd','success');
      }else{
        console.log(err)
        console.log('erro dentro do bulk')
        mainWindow.send('importarDadosEnd','error');
      }
    })
  }else{
    console.log('não tem db')
    mainWindow.send('importarDadosEnd','error');
  }
  
})

ipcMain.on('uploadCancelado',(event) => {
  mainWindow.send('uploadCancelado');
});