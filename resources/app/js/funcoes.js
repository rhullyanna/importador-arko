
const app = require('electron').remote.app;
const { remote } = require('electron'); 
const electron = require('electron');
const { ipcRenderer } = electron;

require( 'datatables.net-dt' )();

function janela_close(){
    $("#fecha-janela").click(function(){
        // remote.BrowserWindow.getFocusedWindow().close();
        app.quit();
        // console.log('fechar janela');
    });
}

function janela_max(){
    $("#max-janela").click(function(){
        if(remote.BrowserWindow.getFocusedWindow().isMaximized() == true){
            remote.BrowserWindow.getFocusedWindow().restore();
        }else{
            remote.BrowserWindow.getFocusedWindow().maximize();
        }
    });
}

function janela_min(){
    $("#min-janela").click(function(){
        if(remote.BrowserWindow.getFocusedWindow().isMinimized() == true){
            remote.BrowserWindow.getFocusedWindow().restore();
        }else{
            remote.BrowserWindow.getFocusedWindow().minimize();
        }
    });
}

function ini(){
    janela_close();
    janela_max();
    janela_min();
}

function loading(){
    swal({
        title: "Loading...",
        id:'load-modal',
        onOpen: async () => {
            swal.showLoading()
            await new Promise(r => setTimeout(r, 500));
            timerInterval = setInterval(() => {
            },100)
        },
    });
}

function encerraLoading(){
    swal.close();
}

function sweetAlert2(tipo, titulo ,texto){
    swal({
        title: titulo,
        text: texto,
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success btn-fill",
        type: tipo
    }).catch(swal.noop)
}

function sweetAlert(tipo, titulo ,texto){
    swal({
        title:  titulo,
        type:   tipo,
        html:   texto,
        showCloseButton: false,
        showCancelButton: false,
        focusConfirm: true,
        confirmButtonText:
          '<i class="fa fa-thumbs-up"></i> Ok!',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        preConfirm: function () {
          
        }
    });

    // swal({
    //     title: titulo,
    //     text: texto,
    //     buttonsStyling: false,
    //     confirmButtonClass: "btn btn-success btn-fill",
    //     type: tipo
    // }).catch(swal.noop);
}

$(document).ready(function(){
    
    ini();

});