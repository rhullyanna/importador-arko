

var janelaUploadArquivo = new Vue({
    el:'#janelaUploadArquivo',
    data : {
        lojas: [
            // {id: "34", nome: "Medical"},
            // {id: "5", nome: "DB"},
            // {id: "88", nome: "Roma"}
        ],
        inventarios: [
            // {id:"22", nome:"Inventario 1"},
            // {id:"22", nome:"Inventario 2"}
        ],
        lojaSelecionada: [],
        inventarioSelecionado: []
    },
    methods: {
        selecionaLoja: function (selecionado){
            console.log(selecionado);
            getInventarios(this.lojaSelecionada.id);
        },
        selecionaInventario: function (){
            selecionados.push(selecionado);
        },
        fecharJanelaUpload: function(){
            ipcRenderer.send('uploadCancelado');
            fecharJanelaUpload();
        }
    },
    created(){
        loading();
        ipcRenderer.send('getLojas');
        montaSelectLojas();
        montaSelectInventarios();
    }
});

function getInventarios(loja_id){
    loading();
    ipcRenderer.send('getInventarios',loja_id);
}

function montaSelectInventarios(){
    ipcRenderer.on('setInventarios', (event, inventarios) => {
        janelaUploadArquivo.inventarios = inventarios;
        encerraLoading();
    });
}

function enviaArquivo(){
    let doc;
    let camposPreenchidos = true;
    let selecionados = [];

    document.querySelector('form').addEventListener('submit', (event) => {
        event.preventDefault();
        console.log(janelaUploadArquivo.lojaSelecionada.length);
        if((janelaUploadArquivo.lojaSelecionada.length == 0) || (janelaUploadArquivo.inventarioSelecionado.length == 0)){
            camposPreenchidos = false;
        }else{
            camposPreenchidos = true;
        }
        if(camposPreenchidos != true){
            sweetAlert('info','Campos vazios','Selecione os campos de loja e inventario antes de importar o arquivo.');
        }else{
            selecionados.push(janelaUploadArquivo.lojaSelecionada);
            selecionados.push(janelaUploadArquivo.inventarioSelecionado);

            const { path } = document.querySelector('input').files[0];
            // ipcRenderer.send('ativarLoading', doc);
            // ipcRenderer.send('lojaEinventario', selecionados);
            ipcRenderer.send('arquivoImportado', selecionados ,path);
            fecharJanelaUpload();
        }
        
    });
    
    
}

function erroRequisicao(){
    ipcRenderer.on('erroRequisicao',(event) => {
        encerraLoading();
        sweetAlert(
            'warning', 
            'Erro de conexão' , 
            'Algo de errado ocorreu com a consulta ao sistema web. Por favor rerifique se o computador está conectado à internet, tente mais tarde ou contate os responsáveis.'
        );
    });
}

function fecharJanelaUpload(){
    remote.BrowserWindow.getFocusedWindow().close();
}

function montaSelectLojas(){
    ipcRenderer.on('setLojas', (event, lojas) => {
        janelaUploadArquivo.lojas = lojas;
        encerraLoading();
    });
}

function iniFormArquivo(){
    
    enviaArquivo();
    erroRequisicao();
}

function paginarTabela(){
    $(document).ready(function(){
        $('#tabelaRegistros').DataTable({
            "language": {
                  "lengthMenu": "Mostrando _MENU_ registros por página",
                  "zeroRecords": "Nada encontrado",
                  "info": "Mostrando página _PAGE_ de _PAGES_",
                  "infoEmpty": "Nenhum registro disponível",
                  "infoFiltered": "(filtrado de _MAX_ registros no total)"
              }
          });
    });
}

$(document).ready(function(){
    
    iniFormArquivo();

});