
var janelaLoginVue = new Vue({
    el:'#janelaLogin',
    data : {
        registros: [],
    },
    methods: {
        fecharJanelaLogin: function(){
            fecharJanelaLogin();
        },
        realizarLogin: function(){
            realizarLogin();
        }
    }
});

function fecharJanelaLogin(){
    remote.BrowserWindow.getFocusedWindow().close();
}

function realizarLogin(){
    let lUsuario = $("#usuario").val();
    let lSenha = $("#senha").val();
    let login = [{usuario: lUsuario, senha: lSenha}];
    ipcRenderer.send('realizarLogin', login);
}

function respostaLogin(){
    ipcRenderer.on('loginInexistente',(event) => {
        sweetAlert2(
            'warning',
            'Acesso negado',
            'Usuário ou senha inválidos.'
        );
    });
}

function verificaRetornoLogin(){
    ipcRenderer.on('loginExistente', (event, result) => {
        fecharJanelaLogin();
    });

    respostaLogin();
}

$(document).ready(function (){
    verificaRetornoLogin();
    // $('#usuario').mask('999.999.999-99');
});