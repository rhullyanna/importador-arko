
// var db = new PouchDB('importacaoTeste');
var db = new PouchDB('importacaoArko4');

var remoteCouch = false;
let dados;

let dados_para_pouch;

let arrayCodBarras;
let arrayDescItem;

let erros_valoresVazios = [];
let erros_zerosEsquerda = [];
let erros_codigoInternoInvalido = [];
let erros_duplicidadeDescricao = [];
let erros_duplicidadeCodBarras = [];

let verificou_os_registros = false;

// #####################################

// SEMPRE QUE FOR TROCAR DE AMBIENTE, DROPAR A BASE DA APLICAÇÃO, PARA QUE OS REGISTROS NÃO REPLIQUEM EM BASES DIFERENTES


// teste
//  var remoteCouch = 'https://11484c5d-c670-4537-a549-df992c9dac2c-bluemix:3ed768cf71279dbf7392f874cb27989a55cfba8bcb492c029427f4e1aa967371@11484c5d-c670-4537-a549-df992c9dac2c-bluemix.cloudantnosqldb.appdomain.cloud/arko_server';

//################# produção NOVO - apenas registros validos ############
// var remoteCouch = 'https://ibandifferfacriessuffery:69bc665b3dd35ca25897691b595ddccf2128ea04@11484c5d-c670-4537-a549-df992c9dac2c-bluemix.cloudantnosqldb.appdomain.cloud/arko_server_producao';

// usuario: ibandifferfacriessuffery
// senha: 69bc665b3dd35ca25897691b595ddccf2128ea04


// banco abaixo foi usado no inventario 26/12/2019 ####################
// var remoteCouch = 'https://assdarviesouringaitherte:9c28cd7b71f8097dac263bd7a12da53c965602f0@11484c5d-c670-4537-a549-df992c9dac2c-bluemix.cloudantnosqldb.appdomain.cloud/arko_server_prod_2';
// assdarviesouringaitherte
// 9c28cd7b71f8097dac263bd7a12da53c965602f0

// #####################################

const colunas = [
    {
        label: 'Cod. Barras',
        field: 'cod_barras',
        filterable: true,
    },
    {
        label: 'Cod. Interno',
        field: 'cod_interno',
        filterable: true,
    },
    {
        label: 'Desc. Item',
        field: 'desc_item',
        filterable: true,
    },
    {
        label: 'Departamento',
        field: 'departamento',
        filterable: true,
    },
    {
        label: 'Setor/Seçao',
        field: 'setor_secao',
        filterable: true,
    },
    {
        label: 'Desc. setor/seção',
        field: 'desc_setor_secao',
        filterable: true,
    },
    {
        label: 'Grupo',
        field: 'grupo',
        filterable: true,
    },
    {
        label: 'Familia',
        field: 'familia',
        filterable: true,
    },
    {
        label: 'Subfamilia',
        field: 'subfamilia',
        filterable: true,
    },
    {
        label: 'Referencia',
        field: 'referencia',
        filterable: true,
    },
    {
        label: 'Saldo/qtd estoque',
        field: 'saldo_qtd_estoque',
        filterable: true,
    },
    {
        label: 'Valor custo',
        field: 'valor_custo',
        filterable: true,
    },
    {
        label: 'Valor venda',
        field: 'valor_venda',
        filterable: true,
    }
];

var appNovoregistro = new Vue({
    el:'#novoArquivo',
    components: {
        'vue-good-table': require('vue-good-table').VueGoodTable,
    },
    data : {
        usuario: {nome: 'default'},
        registros: [],
        erros: [],
        registros_base: [],
        erros_valoresVazios : [],
        erros_valoresVaziosNovo : [],
        erros_zerosEsquerda : [],
        erros_codigoInternoInvalido : [],
        erros_duplicidadeDescricao : [],
        erros_duplicidadeCodBarras : [],
        filtros_selecionados: [
            'Cod. Barras',
            'Cod. Interno',
            'Desc. Item',
        ],
        columns : colunas,
        filtros:[
            {nome: 'Cod. Barras',       obrigatorio:true, nome_id:'cod_barras'},
            {nome: 'Cod. Interno',      obrigatorio:true, nome_id:'cod_interno'},
            {nome: 'Desc. Item',        obrigatorio:true, nome_id:'desc_item'},
            {nome: 'Departamento',      obrigatorio:false, nome_id:'departamento'},
            {nome: 'Setor/Seçao',       obrigatorio:false, nome_id:'setor_secao'},
            {nome: 'Desc. setor/seção', obrigatorio:false, nome_id:'desc_setor_secao'},
            {nome: 'Grupo',             obrigatorio:false, nome_id:'grupo'},
            {nome: 'Familia',           obrigatorio:false, nome_id:'familia'},
            {nome: 'Subfamilia',        obrigatorio:false, nome_id:'subfamilia'},
            {nome: 'Referencia',        obrigatorio:false, nome_id:'referencia'},
            {nome: 'Saldo/qtd estoque', obrigatorio:false, nome_id:'saldo_qtd_estoque'},
            {nome: 'Valor custo',       obrigatorio:false, nome_id:'valor_custo'},
            {nome: 'Valor venda',       obrigatorio:false, nome_id:'valor_venda'}
        ],
    },
    methods: {
        getusuariologado: function(){
            setUsuarioLogado();
            getUsuarioLogado();
        },
        removerAnotacaoErro: function (arrayList, index){
            switch(arrayList){
                case 'valoresVazios'        : this.erros_valoresVaziosNovo.splice(index, 1); break;
                case 'zerosEsquerda'        : this.erros_zerosEsquerda.splice(index, 1); break;
                case 'codigoInternoInvalido': this.erros_codigoInternoInvalido.splice(index, 1); break;
                case 'duplicidadeDescricao' : this.erros_duplicidadeDescricao.splice(index, 1); break;
                case 'duplicidadeCodBarras' : this.erros_duplicidadeCodBarras.splice(index, 1); break;
            }
        },
        exportarErros: function(){
            exportarErros();
        },
        exportarTabelaformatada: function(){
            // metodo que salva o arquivo importado em um txt json com todos os registros
            exportarTabelaformatada();
        },
        marcarFiltro: function(e){
            // marcarFiltro(e);            
        },
        limparTabelas: function (){
            limparDados();
        },
        verificaDados: function(){
            loading();
            this.erros = [];
            this.erros_valoresVaziosNovo = [];
            this.erros_zerosEsquerda = [];
            this.erros_codigoInternoInvalido = [];
            this.erros_duplicidadeDescricao = [];
            this.erros_duplicidadeCodBarras = [];

            verificaDados(appNovoregistro, this.registros, this.filtros_selecionados);
            encerraLoading();            
        },
        abrirJanelaAddArquivo: function(){
            loading();
            this.registros = [];
            this.erros = [];
            limparDados();
            ipcRenderer.send('abrirJanelaAddArquivo');
        },
        persistirDados: function(){
            if(verificaSeTemErros() == false){
                if(verificaSeTemRegistros() == true){
                    if(verificou_os_registros == true){
                        swal({
                            title: "Loading...",
                            id:'load-modal',
                            onOpen: () => {
                                swal.showLoading()
                                timerInterval = setInterval(() => {
                                },100)
                            },
                        });
                        salvarRegistros(this.registros, function callback(err, result){});
                    }else{
                        let doc = {acao: 'persistir_dados', erro:'verificar registro', data: new Date()};
                        ipcRenderer.send('registrarLog',doc);
                        sweetAlert2('info','Verificar Registros','Antes de salvar os registros, verifique se os mesmos contem erros.')
                    }                    
                }else{
                    let doc = {acao: 'persistir_dados', erro:'importar sem registros', data: new Date()};
                    ipcRenderer.send('registrarLog',doc);
                    sweetAlert2('info','Sem Registros','Não foi possivel encontrar registros a serem salvos');
                }
            }else{
                let doc = {acao: 'persistir_dados', erro:'importar com erros', data: new Date()};
                ipcRenderer.send('registrarLog',doc);
                sweetAlert2('info','Dados Inconsistentes','Existem registros com erros, só é possivel salvar os registros caso não existam erros.');
            }
        },
        sincronizaBaseServidorComBaseLocal: function (){
            loading();
            verificarAlteracoes();
        },
        sincronizarBaseLocalNoServidor: function (){
            loading();
            replicarDadosLocaisNoServidor();
        }
    }
});

function setUsuarioLogado(){
    ipcRenderer.on('setUsuarioLogado', (event, usuario) => {
        appNovoregistro.usuario = usuario;
    });
}

function getUsuarioLogado(){
    ipcRenderer.send('getUsuarioLogado');
}

function limparDados(){
    appNovoregistro.registros = [];
    appNovoregistro.erros = [];
    appNovoregistro.erros_valoresVaziosNovo = [];
    appNovoregistro.erros_zerosEsquerda = [];
    appNovoregistro.erros_codigoInternoInvalido = [];
    appNovoregistro.erros_duplicidadeDescricao = [];
    appNovoregistro.erros_duplicidadeCodBarras = [];
    verificou_os_registros = false;
}

function verificaSeTemRegistros(){
    if(appNovoregistro.registros.length > 0){
        return true;
    }
    return false;
}

function exportarErros(){
    let json_erros = appNovoregistro.erros;
    ipcRenderer.send('exportarErros', json_erros);
    ipcRenderer.on('exportarErros',(event, tipo, titulo ,texto) => {
        sweetAlert2(tipo, titulo, texto);
    })

}

function exportarTabelaformatada(){
    let json = appNovoregistro.registros;
    ipcRenderer.send('exportarTabelaformatada', json);
    ipcRenderer.on('exportarTabelaformatada',(event, tipo, titulo ,texto) => {
        sweetAlert2(tipo, titulo, texto);
    })

}

function escondeColunaTabela(id_filtro){

    let lista_de_elementos = document.getElementsByClassName('col_'+id_filtro+'_registros');
    let lista_de_elementos_tab_erros = document.getElementsByClassName('col_'+id_filtro+'_erros');

    for(let i = 0; i< lista_de_elementos.length; i++){
        lista_de_elementos[i].classList.add('esconde');
    }

    for(let i = 0; i< lista_de_elementos_tab_erros.length; i++){
        lista_de_elementos_tab_erros[i].classList.add('esconde');
    }

}

function mostraColunaTabela(id_filtro){

    let lista_de_elementos = document.getElementsByClassName('col_'+id_filtro+'_registros');
    let lista_de_elementos_tab_erros = document.getElementsByClassName('col_'+id_filtro+'_erros');

    for(let i = 0; i< lista_de_elementos.length; i++){
        lista_de_elementos[i].classList.remove('esconde');
    }

    for(let i = 0; i< lista_de_elementos_tab_erros.length; i++){
        lista_de_elementos_tab_erros[i].classList.remove('esconde');
    }

}

function marcarFiltro(e){

    let elemento_registro   = $("#col_"+e.id+"_registros");
    let elemento_erro       = $("#col_"+e.id+"_erros")
    let classe_esconder = 'esconde';

    if(elemento_registro.hasClass(classe_esconder)){
        elemento_registro.removeClass(classe_esconder);
        mostraColunaTabela(e.id);
    }else{
        escondeColunaTabela(e.id);
        elemento_registro.addClass(classe_esconder);
    }

    if(elemento_erro.hasClass(classe_esconder)){
        elemento_erro.removeClass(classe_esconder);
    }else{
        elemento_erro.addClass(classe_esconder);
    }    
}

function recebePlanilhaEmJson(){
    ipcRenderer.on('planilhaEmJson', (event, plan_json) => {
        montaPlanilhaVue(appNovoregistro, plan_json);
        dados_para_pouch = plan_json;
        // console.log(dados_para_pouch);
        // appNovoregistro.filtros_selecionados.forEach(element => {
        //     console.log(element);
        // });
        // paginarTabela();        
    });

    ipcRenderer.on('erroPlanilha', (event) => {
        encerraLoading();
        sweetAlert2('info', 'Planilha Fora do padrão' ,'Por favor, ajuste a disposição das colulas de acordo com o layout.');
        // sweetAlert('info', 'Planilha Fora do padrão' ,'Por favor, ajuste a disposição das colulas de acordo com o layout.');
    });

    ipcRenderer.on('uploadCancelado', (event) => {
        encerraLoading();
    })
    
}

function montaPlanilhaVue(appVue, dados){
    for(let i = 0; i < dados.length; i++){
        appVue.registros.push(dados[i]);
        if(i == (dados.length - 1)){
            encerraLoading();
        }
    }
}

function registroComLinhaDeErro(registro, linha){

    let novoRegistro = {
        'linha':linha.toString(),
        'cod_barras':registro.cod_barras,
        'cod_interno':registro.cod_interno,
        'departamento':registro.departamento,
        'descricao_item':registro.descricao_item,
        'descricao_setor_secao':registro.descricao_setor_secao,
        'familia':registro.familia,
        'grupo':registro.grupo,
        'referencia':registro.referencia,
        'saldo_qtd_estoque':registro.saldo_qtd_estoque,
        'setor_secao':registro.setor_secao,
        'subfamilia':registro.subfamilia,
        'valor_custo':registro.valor_custo,
        'valor_venda':registro.valor_venda,
    };
    // return JSON.stringify(novoRegistro);
    return novoRegistro;
}

// faz a validação dos registros para add ou não na tabela de erros
function verificaDados(appVue, dados, filtros ,callback){

    let nErros = 0;
    montaArrayDeCodBarras(dados);
    montaArrayDeDescricaoItem(dados);

    for(let i = 0; i < dados.length; i++){
        let errado = null;
        let linhaRegistro = (i+2);        

        if(verificaSeRegistroTemValorVazio(dados[i], filtros) == true){
            // add na lista unica com todos os erros
            errado = registroComLinhaDeErro(dados[i], linhaRegistro);
            // add na lista de erros especificos de linha vazia
            appVue.erros_valoresVaziosNovo.push(registroComLinhaDeErro(dados[i], linhaRegistro));
        }

        if(codigoInternoValido(dados[i]) == false){
            errado = registroComLinhaDeErro(dados[i], linhaRegistro);
            // console.log(registroComLinhaDeErro(dados[i], linhaRegistro));
            appVue.erros_codigoInternoInvalido.push(registroComLinhaDeErro(dados[i], linhaRegistro));
        }

        if(temDuplicidadeCodBarras(dados[i].cod_barras) == true){
            errado = registroComLinhaDeErro(dados[i], linhaRegistro);

            appVue.erros_duplicidadeCodBarras.push(registroComLinhaDeErro(dados[i], linhaRegistro));
        }

        // if(temDuplicidadeDescricaoItem(dados[i].descricao_item) == true){
        //     errado = registroComLinhaDeErro(dados[i], linhaRegistro);
        //     appVue.erros_duplicidadeDescricao = registroComLinhaDeErro(dados[i], linhaRegistro);
        // }

        // retirada validação de zeros a esquerda por determinação do contratate
        // if(temZerosEquerda(dados[i].cod_barras) == true){
        //     errado = registroComLinhaDeErro(dados[i], linhaRegistro);
        //     appVue.erros_zerosEsquerda.push(registroComLinhaDeErro(dados[i], linhaRegistro));
        // }

        if(errado != null){
            appVue.erros.push(errado);
            nErros++;
        }
    }
    verificou_os_registros = true;
}

function iniNewArquivo(){
    // loading();
    // verificarAlteracoes();
    recebePlanilhaEmJson();
    setUsuarioLogado();
    getUsuarioLogado();
    importarDadosEnd();
}

function importarDadosEnd(){
    ipcRenderer.on('importarDadosEnd',(event,msg)=>{
        if(msg == 'success'){
            sweetAlert2('success','Concluído','Registros importados com sucesso!');
        }else{
            sweetAlert2('Danger','erro','falha ao importar!');
        }
        
    })
}

function paginarTabela(){
    $(document).ready(function(){
        $('#tabelaRegistros').DataTable({
            "language": {
                  "lengthMenu": "Mostrando _MENU_ registros por página",
                  "zeroRecords": "Nada encontrado",
                  "info": "Mostrando página _PAGE_ de _PAGES_",
                  "infoEmpty": "Nenhum registro disponível",
                //   "infoFiltered": "(filtrado de _MAX_ registros no total)"
              }
          },function(){
            encerraLoading();
          });
    });
}

function verificaSeTemErros(){
    if(appNovoregistro.erros.length > 0){
        return true;
    }
    return false;
}

function verificarAlteracoes(){
    let url_server_pouch = 'https://arkodb-server.herokuapp.com/arko_server';
    let db_server = new PouchDB(url_server_pouch);

    // verifica se a base está criada
    db_server.info().then( (response) => {
        console.log('responsa da info');
        console.log(response);
        // caso a base esteja vazia ele replica a base local no servidor, 
        // caso contratio, replica a base do servidor na local
        if(response.doc_count == 0){
            console.log('sem registros');
            replicarDadosLocaisNoServidor();
        }else{
            console.log('contem registros');
            db.replicate.from(url_server_pouch).on('complete', () => {
                console.log('terminou de replicar o banco');
            }).then(()=>{
                encerraLoading();
            })
        }
    })

    
}

function replicarDadosLocaisNoServidor(){

// #################### estrutura sync com o cloudant IBM #############
    if(remoteCouch){
        sync();
    }
// // #################################################################

    // let url_server_pouch = 'https://arkodb-server.herokuapp.com/arko_server';

    // db.replicate.to(url_server_pouch).then(function (){
    //     encerraLoading();
    // }).then(()=>{
    //     sweetAlert2('success', 'Salvo com sucesso', 'Registros persistidos e prontos para realização do inventario.');
    // }); 
}

// #################### estrutura sync com o cloudant IBM #############
function sync() {

    var opts = {live: true};
    db.sync(remoteCouch, opts, syncError);
    db.replicate.to(remoteCouch, opts, syncError);
    db.replicate.from(remoteCouch, opts, syncError);

}

function syncError() {
    console.log('erro');
}

//   #####################################################################

function salvarRegistros(json_dados, callback) {
    
    // funcional
    // db.bulkDocs(json_dados, function callback(err, result){
    //     if(!err){
    //         sync();
    //         encerraLoading();
    //     }
    // });

    ipcRenderer.send('importarDados',json_dados);


    // não salva sequencialmente
    // definir uma forma de identificação para os registros ou como/por qual info vão ser buscados(ID)
    
    // db.replicate.from(url_server_pouch).on('complete', function(info) {
    //     console.log("trouxe os dados");
    //     db.bulkDocs(json_dados, function callback(err, result) {
    //         // db.bulkDocs(dados_para_pouch, function callback(err, result) {
                
    //         if (!err) {
    //             console.log('Successfully posted a todo!');
    //             db.changes({
    //                 since: 0,
    //                 include_docs: true
    //                 }).then(function (changes) {
    //                 db.replicate.to(url_server_pouch).then(function (){
    //                     encerraLoading();
    //                 }).then(()=>{
    //                     sweetAlert2('success', 'Salvo com sucesso', 'Registros persistidos e prontos para realização do inventario.');
    //                 });                
    //                 console.log("após instrução de replicação")
    //                 console.log(changes)
    //                 }).catch(function (err) {
    //                 console.log(err)
    //                 });
    //         }
    //     }); 

    // });
      
}

function mostrarRegistros() {
    db.allDocs({include_docs: true, descending: true}, function(err, doc) {
        doc.rows.forEach(registro => {
            appNovoregistro.registros_base.push(registro);
            console.log(registro.doc);
        });
        // console.log(doc.rows);
    });
}

// ids_inventarios é um array
function pegaRegistrosInventarios(ids_inventarios){

    db.find({
        selector: {
            // os valores no array, representam os valores do inventario
          inventario: {$in:ids_inventarios}
          }
      }).then(function (result) {
        console.log(result.docs);
      }).catch(function (err) {
        // ouch, an error
        console.log(err);
      });
}

// validações simples

function verificaSeRegistroTemValorVazio(registro, filtros){

    // o filtro diminui 1, pois o ultimo elemento é o observer adicionado pelo vue
    for(let i = 0; i< filtros.length; i++){
        switch(filtros[i]){
            case 'Cod. Barras': 
                // console.log(filtros[i]);
                if(ehVazio(registro.cod_barras) == true){
                    return true;
                }
                break;
            case 'Cod. Interno': 
                // console.log(filtros[i]);
                if(ehVazio(registro.cod_interno) == true){
                    return true;
                }
                
                break;
            case 'Loja': 
                // console.log(filtros[i]);
                if(ehVazio(registro.loja) == true){
                    return true;
                }
                
                break;
            case 'Departamento': 
                // console.log(filtros[i]);
                if(ehVazio(registro.departamento) == true){
                    return true;
                }
                
                break;
            case 'Setor/Seçao': 
                // console.log(filtros[i]);
                if(ehVazio(registro.setor_secao) == true){
                    return true;
                }
                
                break;
            case 'Desc. setor/seção': 
                // console.log(filtros[i]);
                if(ehVazio(registro.descricao_setor_secao) == true){
                    return true;
                }
                
                break;
            case 'Grupo': 
                // console.log(filtros[i]);
                if(ehVazio(registro.grupo) == true){
                    return true;
                }
                
                break;
            case 'Familia': 
                // console.log(filtros[i]);
                if(ehVazio(registro.familia) == true){
                    return true;
                }
                
                break;
            case 'Subfamilia': 
                // console.log(filtros[i]);
                if(ehVazio(registro.subfamilia) == true){
                    return true;
                }
                
                break;
            case 'Referencia': 
                // console.log(filtros[i]);
                if(ehVazio(registro.referencia) == true){
                    return true;
                }
                
                break;
            case 'Saldo/qtd estoque': 
                // console.log(filtros[i]);
                if(ehNumericoVazio(registro.saldo_qtd_estoque) == true){
                    return true;
                }
                
                break;
            case 'Valor custo': 
                // console.log(filtros[i]);
                if(ehNumericoVazio(registro.valor_custo) == true){
                    return true;
                }
                
                break;
            case 'Valor venda': 
                // console.log(filtros[i] +": "+registro.valor_venda);
                if(ehNumericoVazio(registro.valor_venda) == true){
                    return true;
                }
                
                break;
            case 'Desc. Item': 
                // console.log(filtros[i]);
                if(ehVazio(registro.descricao_item) == true){
                    return true;
                }
                
                break;
        }
    }

    return false;
}

function ehNumericoVazio(valor){
    if((valor == "") != (valor == 0) ){
        // console.log('vazio: '+valor);
        return true;
    }

    if(valor == NaN){
        // console.log('Nan: '+ valor);
        return true;
    }

    if(valor == "-"){
        // console.log('traço: '+ valor);
        return true;
    }

    if((valor == undefined) && (valor != "null")){
        // console.log('undefined: '+valor);
        return true;
    }

    if(valor == null){
        // console.log('nullo: '+ valor);
        return true;
    }

    return false;
}

function ehVazio(valor){

    if((valor == "") && (valor == 0)){
        // console.log('vazio: '+valor);
        return true;
    }

    if(valor == NaN){
        // console.log('Nan');
        return true;
    }

    if(valor == "-"){
        // console.log('traço');
        return true;
    }

    if((valor == undefined) && (valor != "null")){
        // console.log('undefined: '+valor);
        return true;
    }

    if(valor == null){
        // console.log('nullo');
        return true;
    }

    return false;
}

function temZerosEquerda(valor){
    let valorEmArray = valor.toString().split('');
    if(valorEmArray.length > 0){
        if(valorEmArray[0] == "0"){
            return true;
        }
    }
    return false;
}

function codigoInternoValido(valor){
    let qtd_caracteres = "";
    if(valor){
        let cod_interno_aux = ""+valor.cod_interno+"";
        qtd_caracteres = cod_interno_aux.toString().length;
    }
    
    if(qtd_caracteres > 20){
        // console.log(valor.cod_interno+" - "+qtd_caracteres);
        return false;
    }
    return true;
}

// popula uma variavel array de escopo global com todos os valores de codigos de barra do arquivo
function montaArrayDeCodBarras(registros){
    arrayCodBarras = [];
    registros.forEach(registro => {
        arrayCodBarras.push(registro.cod_barras);
    });
}

// popula uma variavel array de escopo global com todos os valores de descricao dos itens do arquivo
function montaArrayDeDescricaoItem(registros){
    arrayDescItem = [];
    registros.forEach(registro => {
        arrayDescItem.push(registro.descricao_item);
    });
}

// verifica duplicidade do valor de codigo de barras
// executa metodo js de verificação de indice em um array, caso aquele valor exista no array, existe duplicidade
function temDuplicidadeCodBarras(valor){
    let temDuplicado = false;
    let contRepeticoes = 0;

    arrayCodBarras.forEach(codigo => {
        if(codigo == valor){
            contRepeticoes++;
        }
        if(contRepeticoes > 1){
            temDuplicado = true;
        }
    });

    return temDuplicado;
}

// verifica duplicidade do valor de descricao de itens
// executa metodo js de verificação de indice em um array, caso aquele valor exista no array, existe duplicidade
function temDuplicidadeDescricaoItem(valor){
    let temDuplicado = false;
    let contRepeticoes = 0;

    arrayDescItem.forEach(codigo => {
        if(codigo == valor){
            contRepeticoes++;
        }
        if(contRepeticoes > 1){
            temDuplicado = true;
        }
    });

    return temDuplicado;
}

$(document).ready(function(){
    
    iniNewArquivo();

});





